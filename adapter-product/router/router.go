package router

import (
	"adapter-product/config"
	"adapter-product/handler"
	"adapter-product/repository"
	"adapter-product/service"
	"github.com/gorilla/mux"
	"net/http"
)

func NewRooter(rooter *mux.Router) *mux.Router {

	NewProductHandler(rooter)

	return rooter
}

func NewProductHandler(router *mux.Router) *mux.Router {
	productRepo := repository.NewProductRepository(config.DB)
	var productService = service.NewProductService(productRepo)
	var h = handler.NewProductHandler(productService)

	router.HandleFunc("/v1/products", h.CreateProduct).Methods(http.MethodPost)
	router.HandleFunc("/v1/products", h.GetProducts).Methods(http.MethodGet)

	router.HandleFunc("/v1/products/{id}", h.GetProductByID).Methods(http.MethodGet)

	router.HandleFunc("/v1/products/{id}", h.UpdateProduct).Methods(http.MethodPut)
	router.HandleFunc("/v1/products/{id}", h.DeleteProduct).Methods(http.MethodDelete)
	return router
}
