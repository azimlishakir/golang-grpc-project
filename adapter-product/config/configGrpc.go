package config

import (
	pb "adapter-product/generate/proto"
	"github.com/labstack/gommon/log"
	"google.golang.org/grpc"
	"net"
)

type Server struct {
	pb.ProductServiceServer
}

func InitialGrpc() {
	listener, err := net.Listen("tcp", ":50051")

	if err != nil {
		log.Fatalf("Unable to listen on port :50051: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterProductServiceServer(s, &Server{})
	if err := s.Serve(listener); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

	log.Printf("Hosting server on: %s", listener.Addr().String())
}
