package config

import (
	"adapter-product/repository/model"
)

type Model struct {
	Model interface{}
}

func RegisterModels() []Model {
	return []Model{
		{Model: model.Product{}},
	}
}
