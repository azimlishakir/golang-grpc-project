package handler

import (
	"adapter-product/exception"
	"adapter-product/model"
	"adapter-product/service"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

type ProductHandler struct {
	service service.IProductService
}

func NewProductHandler(productService service.IProductService) *ProductHandler {
	return &ProductHandler{service: productService}
}

func (h *ProductHandler) CreateProduct(w http.ResponseWriter, r *http.Request) {
	var product model.ProductDto
	err := json.NewDecoder(r.Body).Decode(&product)
	if err != nil {
		exception.SendErrorResponse(w, http.StatusBadRequest, fmt.Sprintf("invalid request body"))
		return
	}
	resp, err := h.service.CreateProduct(&product)
	if err != nil {
		exception.SendErrorResponse(w, http.StatusBadRequest, fmt.Sprintf("user can not be created"))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandler) GetProducts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	resp := h.service.GetProducts()
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandler) GetProductByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	id, _ := strconv.ParseInt(params["id"], 10, 64)
	resp, _ := h.service.GetProductById(id)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandler) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	id, _ := strconv.ParseInt(params["id"], 10, 64)
	roleRequest := &model.ProductDto{}
	json.NewDecoder(r.Body).Decode(&roleRequest)
	h.service.UpdateProduct(roleRequest, id)
	json.NewEncoder(w).Encode(roleRequest)
}

func (h *ProductHandler) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	id, _ := strconv.ParseInt(params["id"], 10, 64)
	h.service.DeleteProduct(id)
	w.WriteHeader(http.StatusNoContent)
	json.NewEncoder(w).Encode("The User is Deleted Successfully!")
}
