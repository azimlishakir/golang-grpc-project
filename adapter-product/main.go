package main

import (
	"adapter-product/config"
	"adapter-product/router"
	"github.com/gorilla/mux"
	"github.com/labstack/gommon/log"
	"net/http"
)

func main() {
	config.InitialMigration()
	config.InitialGrpc()
	router := router.NewRooter(mux.NewRouter())
	log.Fatal(http.ListenAndServe("localhost:8080", router))
}
