package model

import "time"

type Product struct {
	Id          int64   `gorm:"size:36;not null;uniqueIndex;primary_key"`
	ProductName string  `gorm:"size:100;not null"`
	Colors      string  `gorm:"size:100;not null"`
	Price       float64 `gorm:"size:100;not null"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
