package repository

import (
	"adapter-product/repository/model"
	"gorm.io/gorm"
)

type IProductRepository interface {
	FindAll(u []*model.Product) ([]*model.Product, error)
	Save(user *model.Product) (*model.Product, error)
	FindByProductId(id int64) (*model.Product, error)
	UpdateProduct(user *model.Product, id int64) (*model.Product, error)
	DeleteProduct(id int64) error
}

type productRepository struct {
	Db *gorm.DB
}

func NewProductRepository(db *gorm.DB) IProductRepository {
	return &productRepository{db}
}

func (pr productRepository) FindAll(product []*model.Product) ([]*model.Product, error) {
	err := pr.Db.Find(&product).Error
	if err != nil {
		return nil, err
	}
	return product, nil

}

func (pr productRepository) Save(product *model.Product) (*model.Product, error) {
	err := pr.Db.Where("product_name =? AND colors =?", product.ProductName, product.Colors).First(&product).Error

	err = pr.Db.Create(&product).Error
	if err != nil {
		return nil, err
	}
	return product, err

}

func (pr productRepository) FindByProductId(id int64) (*model.Product, error) {
	var product model.Product
	err := pr.Db.Where("id = ?", id).First(&product).Error
	if err != nil {
		return nil, err
	}
	return &product, err

}

func (pr productRepository) UpdateProduct(product *model.Product, id int64) (*model.Product, error) {
	var dbProduct model.Product
	var err = pr.Db.Find(&dbProduct, id).Error
	dbProduct.ProductName = product.ProductName
	dbProduct.Colors = product.Colors

	err = pr.Db.Save(&dbProduct).Error

	if err != nil {
		return nil, err
	}
	return product, err

}

func (pr productRepository) DeleteProduct(id int64) error {
	var product model.Product
	err := pr.Db.Delete(&product, id).Error

	if err != nil {
		return err
	}

	return nil
}
