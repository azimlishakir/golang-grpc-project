package model

type ProductDto struct {
	Id          int64   `json:"id"`
	ProductName string  `json:"productName"`
	Colors      string  `json:"colors"`
	Price       float64 `json:"price"`
}
