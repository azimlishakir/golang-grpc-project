package service

import (
	"adapter-product/model"
	"adapter-product/repository"
	model2 "adapter-product/repository/model"
	"github.com/labstack/gommon/log"
)

type IProductService interface {
	CreateProduct(productRequest *model.ProductDto) (*model.ProductDto, error)
	GetProducts() []model.ProductDto
	GetProductById(id int64) (*model.ProductDto, error)
	UpdateProduct(userRequest *model.ProductDto, id int64) *model.ProductDto
	DeleteProduct(id int64)
}

type productService struct {
	productRepo repository.IProductRepository
}

func NewProductService(productRepo repository.IProductRepository) IProductService {
	return &productService{
		productRepo: productRepo,
	}
}

func (p productService) CreateProduct(productRequest *model.ProductDto) (*model.ProductDto, error) {
	log.Info("ActionLog.CreateUser.Start")

	product := &model2.Product{}
	productResponse := &model.ProductDto{}

	product.ProductName = productRequest.ProductName
	product.Colors = productRequest.Colors
	product.Price = productRequest.Price
	productEntity, err := p.productRepo.Save(product)
	if err != nil {
		return nil, err
	}
	productResponse.Id = productEntity.Id
	productResponse.ProductName = productEntity.ProductName
	productResponse.Colors = productEntity.Colors
	productResponse.Price = productEntity.Price
	log.Info("ActionLog.CreateUser.End")
	return productResponse, err
}

func (p productService) GetProducts() []model.ProductDto {
	log.Info("ActionLog.GetUsers.Start")

	products := make([]*model2.Product, 0)

	productResponses := make([]model.ProductDto, 0)

	products, _ = p.productRepo.FindAll(products)

	for i := range products {
		productResponse := model.ProductDto{}

		productResponse.Id = products[i].Id
		productResponse.ProductName = products[i].ProductName
		productResponse.Colors = products[i].Colors
		productResponse.Price = products[i].Price
	}
	log.Info("ActionLog.GetUsers.End")
	return productResponses

}

func (p productService) GetProductById(id int64) (*model.ProductDto, error) {
	log.Info("ActionLog.GetUserById.Start")

	productResponse := &model.ProductDto{}
	product, err := p.productRepo.FindByProductId(id)
	if err != nil {
		return nil, err
	}
	productResponse.Id = product.Id
	productResponse.ProductName = product.ProductName
	productResponse.Colors = product.Colors
	productResponse.Price = product.Price
	log.Info("ActionLog.GetUserById.End")
	return productResponse, err
}

func (p productService) UpdateProduct(userRequest *model.ProductDto, id int64) *model.ProductDto {
	log.Info("ActionLog.UpdateUser.Start")
	product := &model2.Product{}
	productResponse := &model.ProductDto{}

	product.Id = productResponse.Id
	product.ProductName = userRequest.ProductName
	product.Colors = userRequest.Colors
	product.Price = userRequest.Price

	product, _ = p.productRepo.UpdateProduct(product, id)
	productResponse.Id = product.Id
	productResponse.ProductName = product.ProductName
	productResponse.Colors = product.Colors
	productResponse.Price = product.Price
	log.Info("ActionLog.UpdateUser.End")
	return productResponse
}

func (p productService) DeleteProduct(id int64) {
	log.Info("ActionLog.DeleteUser.Start")
	_ = p.productRepo.DeleteProduct(id)
	log.Info("ActionLog.DeleteUser.End")
}
